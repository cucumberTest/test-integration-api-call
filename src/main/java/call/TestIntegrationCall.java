package call;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class TestIntegrationCall {

    private static RestTemplate restTemplate = new RestTemplate();

    @RequestMapping(value = "/call",method = RequestMethod.GET)
    @ResponseBody
    public static String greeting() {
        final String url ="http://localhost:9006/response";
        final String testResponse = restTemplate.getForObject(url, String.class);
        return testResponse;

    }

    @RequestMapping(value = "/status",method = RequestMethod.GET)
    @ResponseBody
    public static String status() {
        final String testResponse = "200";
        return testResponse;

    }

}
